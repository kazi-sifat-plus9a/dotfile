
#!/bin/bash
#set -e
##################################################################################################################
# Author 	: 	kazi sifat
# Website 	: 	https://www.kazisifat.com
##################################################################################################################
#
#   DO NOT JUST RUN THIS. EXAMINE AND JUDGE. RUN AT YOUR OWN RISK.
#
##################################################################################################################
# change into your name and email.

cp ~/.bashrc ~/dotfile
cp ~/.gtkrc-2.0 ~/dotfile
cp ~/.vimrc ~/dotfile
cp ~/.Xresources ~/dotfile
cp ~/.xscreensaver ~/dotfile
cp ~/.zshrc ~/dotfile
cp -r ~/.config ~/dotfile

echo "################################################################"
echo "###################    T H E   E N D      ######################"
echo "################################################################"

